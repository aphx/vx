import * as t from '@babel/types';
import * as BabelCore from '@babel/core';
import syntaxJsx from '@babel/plugin-syntax-jsx';
import { addNamed, isModule, addNamespace } from '@babel/helper-module-imports';
import { NodePath } from '@babel/traverse';

interface State{
  get: (name: string) => any;
  set: (name: string, value: any) => any;
  opts: any;
  file: BabelCore.BabelFile
}

export default ({ types }: typeof BabelCore)=>({
  name:'babel-vx-jsx',
  inherits:syntaxJsx,
  visitor:{
    CallExpression:{
      exit(path:NodePath<t.CallExpression>,state:State){
        console.log(path.node)
        console.log(path.get('expression'))
        console.log(path.findParent(p=>p.is))
        const fname = path.get('expression').node.escapedText;
        if(fname==='vm'||fname==='ref'){
          console.log('find')
        }
        // if(fname.type==='Identifier'&&callee['name']==='vm'){
          
        // }
      }
    },
    JSXElement:{
      exit(path:NodePath<t.JSXElement>,state:State){
        console.log(path.node)
      }
    }
  }
})