function toMap(obj: any) {
  let map = new Map();
  Object.keys(obj).forEach((key) => {
    map.set(key, obj[key]);
  });
  return map;
}

interface vNode {
  type: string | object;
  props: object;
  innerText: string;
  childrenCount: number;
}

type vmIdx = number;
type vNodeIdx = number;

interface PageDesc {
  onLoad?: Function;
  onUpdate?: Function;
  onDestroy?: Function;
}

class Page {
  vmList: any[];
  depMap: Map<vmIdx, vNodeIdx>;
  desc: PageDesc;

  constructor() {
    this.vmList = [];
    this.depMap = new Map();
    this.desc = {};
  }
}

const App = {
  currPageIdx: 0,
  pages: [],
  currPage(): Page {
    return this.pages[this.currPageIdx];
  },
};

export function definePage(setup: Function) {
  App.currPageIdx = App.pages.length;
  const page = new Page();
  App.pages.push(page);
  setup();
}

export function onLoad(f: Function) {
  App.currPage().desc.onLoad = f;
}

export function onUpdate(f: Function) {
  App.currPage().desc.onUpdate = f;
}

export function onDestroy(f: Function) {
  App.currPage().desc.onDestroy = f;
}

export function vm(data: any) {
  const wrapper = {
    _actions: [],
    _initData: toMap(data),
    _oldData: toMap(data),
    _vmIdx: App.currPage().vmList.length,
    sub: function (f: Function) {
      if (typeof f === "function") this._actions.push(f);
      return this;
    },
    ...data,
  };
  const proxy = new Proxy(wrapper, {
    set: function (target, prop, val, receiver) {
      if (
        prop === "_actions" ||
        prop === "_initData" ||
        prop === "_oldData" ||
        prop === "_vmIdx"
      ) {
        throw new TypeError(`can not set ${prop}`);
        return false;
      }
      const oldVal = target._oldData.get(prop);
      if (oldVal !== val) {
        target[prop] = val;
        if (target._actions) {
          target._actions.forEach((f: Function) => {
            f(val, oldVal, target, prop);
          });
        }
        target._oldData.set(prop, val);
      }
      return true;
    },
  });
  App.currPage().vmList.push(proxy);
  return proxy;
}

export const ref = (data: number | string | bigint | boolean | null) =>
  vm({ value: data });

export function h(type: any, config: any, children: any) {
  console.log(arguments);
  let vnodeArr: vNode[] = [
    { type: type, props: {}, innerText: "", childrenCount: 0 },
  ];
  let propName;
  if (config != null) {
    for (propName in config) {
      if (hasOwnProperty.call(config, propName)) {
        vnodeArr[0].props[propName] = config[propName];
      }
    }
  }
  if (type && type.defaultProps) {
    var defaultProps = type.defaultProps;
    for (propName in defaultProps) {
      if (vnodeArr[0].props[propName] === undefined) {
        vnodeArr[0].props[propName] = defaultProps[propName];
      }
    }
  }
  let childrenLength = arguments.length - 2;
  if (childrenLength > 0) {
    // let elIdx = 0;
    for (let i = 0; i < childrenLength; i++) {
      const it = arguments[i + 2];
      if (typeof it === "string") {
        vnodeArr[0].innerText = vnodeArr[0].innerText + it;
        // tryCreateDep(it, 0);
      } else if (typeof it === "object") {
        if (!it.length) {
          Object.keys(it).forEach((k) => {
            const vnode = {
              type: "vtext",
              props: { prop: k, val: it[k] },
              innerText: "",
              childrenCount: 0,
            };
            vnodeArr.push(vnode);
            vnodeArr[0].childrenCount++;
            // tryCreateDep(it[k], vnodeArr.length - 1);
          });
          continue;
        }
        let childArr = it.length ? it : []; //: h.apply(this, [...it]); //[it.type,it.config,it.children]);
        vnodeArr[0].childrenCount += childArr.length;
        vnodeArr = vnodeArr.concat(childArr);
      }
    }
  }
  return vnodeArr;
}
