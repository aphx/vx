import { definePage, onLoad, ref } from "../src";

definePage(() => {
  const counter = ref(0);
  counter.sub((val, oldVal) => {
    console.log(`set to ${val} from ${oldVal}`);
  });
  console.log(counter.value);
  counter.value = 2;
  onLoad(() => {
    counter.value = 3;
  });
});
