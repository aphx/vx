import { defineConfig } from "vite";

export default defineConfig({
  build: {
    lib: {
      entry: "src/index.ts",
      name: "vx-core",
      fileName: (format: string) => `vx-core.${format}.js`,
    },
  },
});
